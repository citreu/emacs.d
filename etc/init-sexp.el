;;; -*- lexical-binding: t; -*-

;;; Sexp manipulation

;; Lispy

(walle-add-hooks '(lisp-mode-hook
                   racket-mode-hook
                   emacs-lisp-mode-hook
                   scheme-mode-hook)
                 #'lispy-mode)

(setq lispy-close-quotes-at-end-p t
      lispy-completion-method #'helm)

(with-eval-after-load 'lispy
  (lispy-set-key-theme '(special lispy))
  (add-hook 'lispy-mode-hook #'turn-off-smartparens-mode))

;; Macrostep

(define-key emacs-lisp-mode-map
  [(control ?c) (control ?e)] #'macrostep-expand)
(define-key lisp-interaction-mode-map
  [(control ?c) (control ?e)] #'macrostep-expand)

(with-eval-after-load 'macrostep
  (define-key macrostep-keymap "J" #'macrostep-next-macro)
  (define-key macrostep-keymap "K" #'macrostep-prev-macro)
  (define-key macrostep-keymap "e" #'macrostep-expand)
  (define-key macrostep-keymap "c" #'macrostep-collapse)
  (define-key macrostep-keymap "q" #'macrostep-collapse-all)
  (add-hook 'before-save-hook #'macrostep-collapse-all))

;;; Emacs Lisp

(setq flycheck-emacs-lisp-load-path 'inherit)

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (setq-local mode-name "Elisp")
            (setq-local flycheck-disabled-checkers '(emacs-lisp-checkdoc))))

(add-hook 'emacs-lisp-mode-hook #'sly-el-indent-setup)

(with-eval-after-load 'elisp-mode
  (elispfl-mode)

  (dolist (x (list emacs-lisp-mode-map lisp-interaction-mode-map))
    (define-key x [(control ?c) (control ?c)] #'eval-defun)
    (define-key x [(control ?c) (control ?b)] #'eval-buffer)))

(add-hook 'ielm-mode-hook #'turn-on-smartparens-strict-mode)

(with-eval-after-load 'ielm
  (elispfl-ielm-mode))

(setq lisp-indent-function #'common-lisp-indent-function)

(defvar ciremacs-elisp-correct-indentation-list
  '((defface . nil)
    (defalias . nil)
    (define-minor-mode . 1)
    (define-derived-mode . 3)
    (defface . 1)
    ;; (unwind-protect . 1)
    (define-globalized-minor-mode . nil)
    ;; Fix `use-pacakge' indentation.
    (use-package . 1)
    (define-key . nil)))

(pcase-dolist (`(,sym . ,spec) ciremacs-elisp-correct-indentation-list)
  (put sym 'common-lisp-indent-function-for-elisp spec))

;;; Common Lisp

(setq inferior-lisp-program
      (cl-dolist (exe '("sbcl"          ;SBCL
                        "ecl"             ;ECL
                        "lx86cl" "wx86cl" ;CCL
                        "clisp"           ;Clisp
                        ))
        (when-let ((it (executable-find exe)))
          (cl-return it))))

(setq sly-command-switch-to-existing-lisp 'always
      sly-ignore-protocol-mismatches t)

(defun ciremacs-sly-cleanup-maybe ()
  "Kill processes and leftover buffers when killing the last sly buffer."
  (unless (cl-loop for buf in (delq (current-buffer) (buffer-list))
                   if (and (buffer-local-value 'sly-mode buf)
                           (get-buffer-window buf))
                     return t)
    (dolist (conn (sly--purge-connections))
      (sly-quit-lisp-internal conn 'sly-quit-sentinel t))
    (let (kill-buffer-hook kill-buffer-query-functions)
      (mapc #'kill-buffer
            (cl-loop for buf in (delq (current-buffer) (buffer-list))
                     if (buffer-local-value 'sly-mode buf)
                       collect buf)))))

(defun ciremacs-sly-initialize ()
  (cond ((or (equal (substring (buffer-name (current-buffer)) 0 1) " ")
             (sly-connected-p)))
        ((executable-find inferior-lisp-program)
         (let ((sly-auto-start 'always))
           (sly-auto-start)
           (add-hook 'kill-buffer-hook #'ciremacs-sly-cleanup-maybe nil t)))
        (t
         (lwarn 'sly :warning "Couldn't find `inferior-lisp-program' (%s)"
                inferior-lisp-program))))

(add-hook 'sly-mode-hook #'ciremacs-sly-initialize)

(clhs-setup)

;;; Guile

;; (setq geiser-default-implementation 'guile)
(setq geiser-guile-manual-lookup-nodes
      '("Guile" "Guile Library" "Guile Reference"))

(geiser-implementation-extension 'guile "scm")

(push (cons (rx "." (or "sls" "sld") eos) 'scheme-mode) auto-mode-alist)
