;;; -*- lexical-binding:t ; -*-

;; Use msys2 bash shell
;; (setq explicit-shell-file-name "bash.exe")

(setq explicit-shell-file-name "bash")

(global-set-key [remap async-shell-command] #'with-editor-async-shell-command)
(global-set-key [remap shell-command] #'with-editor-shell-command)

(provide 'init-shell)
;;; init-shell.el ends here
