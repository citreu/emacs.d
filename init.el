;;; -*- lexical-binding:t ; -*-

;;Without this comment package.el add `package-initialize' here
;; (package-initialize)

;;* Constants Setup

(defvar cm/config-files-directory (expand-file-name "etc" user-emacs-directory)
  "The directory to store configuration files.")

(defvar cm/cache-files-directory (expand-file-name "var" user-emacs-directory)
  "The directory to store the dotfiles create by different extensions.")

(defvar cm/library-files-directory (expand-file-name "lib" user-emacs-directory)
  "The directory to store extensions files, whether from ELPA or Github.")

(defvar cm/third-party-files-directory (expand-file-name "opt" user-emacs-directory)
  "The directory to store third party binary tools.")

(unless (file-directory-p cm/cache-files-directory)
  (mkdir cm/cache-files-directory))

(setq custom-file (expand-file-name "custom.el" cm/config-files-directory))

(when (file-exists-p custom-file) (load custom-file :no-error :no-message))

(defvar cm/gc-cons-threshold-up-limit (* 100 1024 1024))

(defvar cm/gc-cons-threshold-default (* 20 1024 1024))

(defun cm/inc-gc-cons-threshold! ()
  "Increase `gc-cons-threshold' to `cm/gc-cons-threshold-up-limit'."
  (setq gc-cons-threshold cm/gc-cons-threshold-up-limit))

(defun cm/reset-gc-cons-threshold! ()
  "Rest `gc-cons-threshold' to `cm/gc-cons-threshold-default'."
  (setq gc-cons-threshold cm/gc-cons-threshold-default))

;;* Avoid Emacs do GC during the initializing
(let ((default-file-name-handler-alist file-name-handler-alist))
  (cm/inc-gc-cons-threshold!)
  (setq file-name-handler-alist nil)
  (add-hook 'emacs-startup-hook
            (lambda ()
              (setq file-name-handler-alist
                    default-file-name-handler-alist)
              (cm/reset-gc-cons-threshold!)
              (add-hook 'minibuffer-setup-hook
                        #'cm/inc-gc-cons-threshold!)
              (add-hook 'minibuffer-exit-hook
                        #'cm/reset-gc-cons-threshold!)
              (add-hook 'focus-out-hook #'garbage-collect))))


;; I don't like `provide' and `require' mechanism in user configurations
;; It may introduce extra symbols to `features' variable.
(defsubst ciremacs-require (file)
  "Load my config files"
  (load (expand-file-name (symbol-name file)
                          cm/config-files-directory)
        :no-error :no-message))

;; Frequent using built-in packages
(require 'cl-lib)
(require 'subr-x)

;; Package management
(ciremacs-require 'init-package)

;; UI
(ciremacs-require 'init-ui)

;; Basic
(ciremacs-require 'init-basic)
;; (ciremacs-require 'init-ivy)
(ciremacs-require 'init-helm)

;; Better editing
(ciremacs-require 'init-edit)

;; Completions (YAS, Company and others)
(ciremacs-require 'init-completion)

;; Markup Language
(ciremacs-require 'init-org)

;; Programming Language
(ciremacs-require 'init-prog)
(ciremacs-require 'init-sexp)

;; ;; Chinese language support
(ciremacs-require 'init-chinese)

;; ;; File-managemnet
(ciremacs-require 'init-dired)
(ciremacs-require 'init-project)
(ciremacs-require 'init-vc)


;; ;; Applications
(ciremacs-require 'init-shell)

;; Network applications
(ciremacs-require 'init-mail)
(ciremacs-require 'init-net)
