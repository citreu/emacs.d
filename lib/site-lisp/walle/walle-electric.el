;;; -*- lexical-binding: t; -*-

(require 'walle-utils)

(defvar-local walle-electric--indent-words nil
  "A list of words which will trigger indentation.")

(defun walle-electric--indent-word-function (_)
  (when (and (eolp) walle-electric--indent-words)
    (save-excursion
      (backward-word)
      (looking-at-p (rx-to-string `(and bow (or ,@walle-electric--indent-words))
                                  t)))))

;;;###autoload
(cl-defun walle-electric-set! (modes &key chars words)
  (declare (indent 1))
  (dolist (m (walle-enlist modes))
    (let ((fn (lambda ()
                (when (eq major-mode m)
                  (setq-local electric-indent-inhibit nil)
                  (electric-indent-local-mode)
                  (when chars
                    (setq-local electric-indent-chars chars))
                  (when words
                    (setq-local walle-electric--indent-words words)))))
          (hook-var-sym (intern (format "%s-hook" m))))
      (add-hook hook-var-sym fn))))

(provide 'walle-electric)

(with-eval-after-load 'electric
  (add-to-list 'electric-indent-functions
               #'walle-electric--indent-word-function))

;;; walle-electric.el ends here
